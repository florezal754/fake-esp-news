Nombre del proyecto: Fakesp News
Nombre y apellido de los integrantes del equipo: Laura Flórez | Daniel Panadero | Josue David Peñuela Castro
Pequeña descripción del proyecto: Aproximadamente sólo el 14% de la población es capaz de detectar noticias falsas. Este clasificador propone una herramienta para garantizar la confiabilidad del contenido introducido
